#include "packets.h"

#include <stdexcept>

namespace packets {

void CarPosPacket::UpdateBytes(std::vector<unsigned char> bytes) {
  ok_ = true;
  bytes_ = std::vector<unsigned char>(bytes.begin(), bytes.end());
  flying = (bytes_[2] >> 3 & 1) == 0;
}

std::vector<unsigned char> CarPosPacket::GetBytes(uint16_t time) {
  if (!ok_) {
    throw std::runtime_error(
        "CarPosPacket::GetBytes called when packet not ok");
  }

  bytes_[0] = time >> 8;
  bytes_[1] = time & 0xff;

  return bytes_;
}

bool CarPosPacket::ok() { return ok_; }

}  // namespace packets