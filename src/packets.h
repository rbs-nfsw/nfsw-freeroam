#ifndef FREEROAM_PACKETS_H_
#define FREEROAM_PACKETS_H_

#include <cstdint>
#include <vector>

#include "frmath.h"

namespace packets {

struct ServerHeader {
  uint16_t counter;
  uint8_t type;
  uint16_t timediff;
  uint16_t clitime;
  uint16_t counter_frag;
  uint16_t unkn1;
  uint8_t unkn2;
};

enum class ClientSubpacketType : uint8_t {
  kChannelInfo = 0x00,
  kPlayerInfo = 0x01,
  kCarPos = 0x12
};

class CarPosPacket {
 public:
  std::vector<unsigned char> GetBytes(uint16_t time);
  void UpdateBytes(std::vector<unsigned char>);
  bool ok();
  math::Vector2D postition();

 private:
  bool ok_ = false;
  std::vector<unsigned char> bytes_;
  math::Vector2D position_;
  bool flying;

  bool IsLowY();
  unsigned long GetX();
  unsigned long GetY();
};

}  // namespace packets

#endif  // FREEROAM_PACKETS_H_