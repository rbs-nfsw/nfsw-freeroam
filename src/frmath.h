#ifndef VECTOR2D_H
#define VECTOR2D_H

namespace math {

struct Vector2D {
  long x;
  long y;
};

Vector2D Subtract(Vector2D, Vector2D);
Vector2D Absolute(Vector2D);
unsigned long Length(Vector2D);

}  // namespace math

#endif /* VECTOR2D_H */
