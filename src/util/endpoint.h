#ifndef FREEROAM_UTIL_ENDPOINT_H_
#define FREEROAM_UTIL_ENDPOINT_H_
#include <string>

namespace util {

inline std::string endpoint_to_string(boost::asio::ip::udp::endpoint endpoint) {
  std::stringstream ss;
  ss << endpoint.address().to_string();
  ss << ":";
  ss << endpoint.port();
  return ss.str();
}

}  // namespace util

#endif  // FREEROAM_UTIL_ENDPOINT_H_