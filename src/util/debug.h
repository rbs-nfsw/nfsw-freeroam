#ifndef FREEROAM_UTIL_DEBUG_H_
#define FREEROAM_UTIL_DEBUG_H_
#include <iostream>

void debugHex(std::vector<unsigned char> data) {
  for (char unsigned const& b : data) {
    printf("%02X ", b);
  }
  printf("\n");
}

#endif  // FREEROAM_UTIL_DEBUG_H_
