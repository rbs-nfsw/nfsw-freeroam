#include "../stdafx.h"

#include <chrono>
#include <iostream>
#include <system_error>

#include "ByteBuffer.h"
#include "client/UdpClient.h"
#include "client/array_diff.h"

namespace client {

UdpClient::UdpClient(boost::asio::ip::udp::socket& socket_,
                     boost::asio::ip::udp::endpoint sender_endpoint_,
                     ClientMap* clients, uint16_t clitime_,
                     std::vector<std::vector<unsigned char>> replay)
    : socket_(socket_),
      sender_endpoint_(sender_endpoint_),
      string_endpoint_(util::endpoint_to_string(sender_endpoint_)),
      clients_(clients),
      start_(std::chrono::high_resolution_clock::now()),
      last_receive_(start_),
      clitime_(clitime_),
      replay_(replay),
      sequence_(0) {}

uint16_t UdpClient::getSequence() { return sequence_++; }

uint16_t UdpClient::getTimeDiff() {
  auto dur = std::chrono::high_resolution_clock::now() - start_;
  return static_cast<uint16_t>(
      std::chrono::duration_cast<std::chrono::milliseconds>(dur).count());
}

std::string UdpClient::endpoint() { return string_endpoint_; }

bool UdpClient::active() {
  return (std::chrono::high_resolution_clock::now() - last_receive_) <
         std::chrono::milliseconds(5000);
}

void UdpClient::SendHandshakeReply() {
  auto buf = ByteBuffer();
  buf.WriteUint16BE(getSequence());
  buf.WriteByte(0x01);
  buf.WriteUint16BE(getTimeDiff());
  buf.WriteUint16BE(clitime_);
  buf.Write(std::vector<unsigned char>{0x00, 0x00, 0x00, 0x00});
  SendMessage(buf.Bytes());
}

ClientMap UdpClient::GetNearClients() {
  ClientMap nearClients;
  int i = 0;
  for (auto const& pair : *clients_) {
    if (pair.second == this) continue;
    nearClients.insert(pair);
    if (i >= 14) break;
    i++;
  }
  return nearClients;
}

void UdpClient::RecalculateSlots() {
  auto nearClients = GetNearClients();
  auto diff = ArrayDiff(last_near_clients_, nearClients);
  for (auto const& client : diff.removed) {
    slots.RemoveClient(client.first);
  }
  for (auto const& client : diff.added) {
    slots.AddClient(client.second);
  }
  last_near_clients_ = nearClients;
}

void UdpClient::SendPlayerSlots() {
  bool replay = replay_.size() > 0;
  if (!replay) {
    RecalculateSlots();
  }
  auto buf = ByteBuffer();
  auto seq = getSequence();
  buf.WriteUint16BE(seq);
  buf.WriteByte(0x02);
  auto timediff = getTimeDiff();
  buf.WriteUint16BE(timediff);
  buf.WriteUint16BE(clitime_);
  buf.WriteUint16BE(seq);
  buf.Write(std::vector<unsigned char>{0xff, 0xff, 0x00});
  for (size_t i = 0; i < 14; i++) {
    if (replay) {
      if (i < replay_.size()) {
        buf.WriteByte(0x00);
        std::vector<unsigned char> vec(replay_[i]);
        {
          auto iter = vec.begin();
          for (;;) {
            auto type = (packets::ClientSubpacketType)iter[0];
            auto len = (uint8_t)iter[1];
            if (type == packets::ClientSubpacketType::kCarPos) {
              iter[2] = htons(timediff) >> 8;
              iter[3] = htons(timediff) & 0xff;
              break;
            }
            iter += len + 2;
          }
        }
        buf.Write(vec);
        buf.WriteByte(0xff);
      } else {
        buf.Write(std::vector<unsigned char>{0xff, 0xff});
      }
    } else {
      auto slot = slots[i];
      if (slot == nullptr) {
        buf.Write(std::vector<unsigned char>{0xff, 0xff});
      } else if (!slot->client->ok()) {
        buf.Write(std::vector<unsigned char>{0xff, 0xff});
      } else {
        if (slot->just_added) {
          slot->just_added = false;
          buf.Write(slot->client->GetSlotFullPacket(timediff - 15));
        } else {
          buf.Write(slot->client->GetSlotPosPacket(timediff - 15));
        }
      }
    }
  }
  buf.Write(std::vector<unsigned char>{0x01, 0x01, 0x01, 0x01});
  SendMessage(buf.Bytes());
}

void UdpClient::process(std::vector<unsigned char> msg) {
  if (msg.capacity() == 58 && msg[2] == 0x06) {
    return;
  }
  last_receive_ = std::chrono::high_resolution_clock::now();
  auto iter = msg.begin() + 16;
  auto end = msg.end() - 5;
  bool handled = false;
  for (;;) {
    if (iter == end) {
      break;
    }
    auto type = static_cast<packets::ClientSubpacketType>(iter[0]);
    auto len = uint8_t{iter[1]};
    iter += 2;
    std::vector<unsigned char> data(iter, iter + len);
    iter += len;
    switch (type) {
      case packets::ClientSubpacketType::kChannelInfo:
        channelInfo_ = data;
        handled = true;
        break;
      case packets::ClientSubpacketType::kPlayerInfo:
        playerInfo_ = data;
        handled = true;
        break;
      case packets::ClientSubpacketType::kCarPos:
        carPos_.UpdateBytes(data);
        handled = true;
        break;
    }
  }
  if (handled) {
    SendPlayerSlots();
  }
}

bool UdpClient::ok() { return carPos_.ok(); }

std::vector<unsigned char> UdpClient::GetSlotPosPacket(uint16_t time) {
  auto buf = ByteBuffer();
  buf.WriteByte(0x00);
  buf.WriteByte(0x12);
  buf.WriteByte(0x1a);
  buf.Write(carPos_.GetBytes(time));
  buf.WriteByte(0xff);
  return buf.Bytes();
}
std::vector<unsigned char> UdpClient::GetSlotFullPacket(uint16_t time) {
  auto buf = ByteBuffer();
  buf.WriteByte(0x00);

  buf.WriteByte(0x00);
  buf.WriteByte(0x22);
  buf.Write(channelInfo_);

  buf.WriteByte(0x01);
  buf.WriteByte(0x41);
  buf.Write(playerInfo_);

  buf.WriteByte(0x12);
  buf.WriteByte(0x1a);
  buf.Write(carPos_.GetBytes(time));

  buf.WriteByte(0xff);
  return buf.Bytes();
}

}  // namespace client