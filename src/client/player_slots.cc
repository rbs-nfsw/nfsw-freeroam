#include "client/player_slots.h"

#include <string>
#include "client/UdpClient.h"

namespace client {

PlayerSlots::PlayerSlots() {
  for (int i = 0; i < 14; i++) {
    slots_[i] = nullptr;
  }
}

SlotInfo* PlayerSlots::operator[](int i) { return slots_[i]; }

int PlayerSlots::FindEmptySlot() {
  for (int i = 0; i < 14; i++) {
    if (slots_[i] == nullptr) {
      return i;
    }
  }
  return -1;
}

int PlayerSlots::FindSlot(std::string endpoint) {
  for (int i = 0; i < 14; i++) {
    auto val = slots_[i];
    if (val != nullptr && val->endpoint == endpoint) {
      return i;
    }
  }
  return -1;
}

void PlayerSlots::AddClient(UdpClient* client) {
  auto endpoint = client->endpoint();
  auto slot = FindEmptySlot();
  slots_[slot] = new
  SlotInfo{just_added : true, endpoint : endpoint, client : client};
}

void PlayerSlots::RemoveClient(std::string endpoint) {
  auto slot = FindSlot(endpoint);
  delete slots_[slot];
  slots_[slot] = nullptr;
}

}  // namespace client