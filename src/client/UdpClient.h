#ifndef FREEROAM_CLIENT_UDPCLIENT_H_
#define FREEROAM_CLIENT_UDPCLIENT_H_

#include <chrono>
#include <string>

#include "client/client_map.h"
#include "client/player_slots.h"
#include "packets.h"
#include "util/endpoint.h"

namespace client {

class UdpClient;

class UdpClient {
 public:
  UdpClient(boost::asio::ip::udp::socket& socket_,
            boost::asio::ip::udp::endpoint sender_endpoint_, ClientMap* clients,
            uint16_t clitime_, std::vector<std::vector<unsigned char>> replay);

  void SendMessage(std::vector<unsigned char> msg) {
    socket_.async_send_to(boost::asio::buffer(msg), sender_endpoint_,
                          [this](const boost::system::error_code /*ec*/,
                                 size_t /*bytes_sent*/) {});
  }

  std::string endpoint();
  void SendHandshakeReply();
  void SendPlayerSlots();
  uint16_t getSequence();
  uint16_t getTimeDiff();
  bool active();
  bool ok();
  void process(std::vector<unsigned char> msg);
  uint16_t clitime;

 private:
  boost::asio::ip::udp::socket& socket_;
  boost::asio::ip::udp::endpoint sender_endpoint_;
  std::string string_endpoint_;
  ClientMap* clients_;
  uint16_t sequence_;
  std::chrono::high_resolution_clock::time_point start_;
  std::chrono::high_resolution_clock::time_point last_receive_;
  uint16_t clitime_;
  std::vector<unsigned char> channelInfo_;
  std::vector<unsigned char> playerInfo_;
  packets::CarPosPacket carPos_;
  ClientMap last_near_clients_;
  std::vector<std::vector<unsigned char>> replay_;
  PlayerSlots slots;

  ClientMap GetNearClients();
  void RecalculateSlots();
  std::vector<unsigned char> GetSlotPosPacket(uint16_t time);
  std::vector<unsigned char> GetSlotFullPacket(uint16_t time);
};

}  // namespace client

#endif  // FREEROAM_CLIENT_UDPCLIENT_H_
