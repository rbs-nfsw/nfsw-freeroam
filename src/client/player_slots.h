#ifndef FREEROAM_CLIENT_PLAYER_SLOTS_H_
#define FREEROAM_CLIENT_PLAYER_SLOTS_H_

namespace client {

class UdpClient;

struct SlotInfo {
  bool just_added;
  std::string endpoint;
  UdpClient* client;
};

class PlayerSlots {
 public:
  PlayerSlots();
  SlotInfo* operator[](int i);
  void AddClient(UdpClient* client);
  void RemoveClient(std::string endpoint);

 private:
  int FindEmptySlot();
  int FindSlot(std::string address);
  std::array<SlotInfo*, 14> slots_;
};

}  // namespace client

#endif  // FREEROAM_CLIENT_PLAYER_SLOTS_H_