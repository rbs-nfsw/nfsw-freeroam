#ifndef CLIENT_MAP_H
#define CLIENT_MAP_H

#include <string>
#include <unordered_set>
#include "client/UdpClient.h"

namespace client {

// class ClientMap {
//   UdpClient* Get(std::string endpoint);
//   void Add(UdpClient* client);
//   void Remove(std::string endpoint);

//  private:
//   std::unordered_set<UdpClient*> clients_;
// };

class UdpClient;

using ClientMap = std::unordered_map<std::string, UdpClient*>;

}  // namespace client

#endif /* CLIENT_MAP_H */
