#include "client/array_diff.h"

namespace client {

bool has(ClientMap map_, std::string endpoint) {
  auto iter = map_.find(endpoint);
  if (iter == map_.end()) {
    return false;
  }
  return true;
}

ArrayDiffResult ArrayDiff(ClientMap old_, ClientMap new_) {
  ArrayDiffResult out{};
  for (auto const& pair : new_) {
    if (has(old_, pair.first)) {
      out.kept.insert(pair);
    } else {
      out.added.insert(pair);
    }
  }
  for (auto const& pair : old_) {
    if (!has(new_, pair.first)) {
      out.removed.insert(pair);
    }
  }
  return out;
}

}  // namespace client