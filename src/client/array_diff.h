#ifndef FREEROAM_ARRAY_DIFF_H
#define FREEROAM_ARRAY_DIFF_H

#include <unordered_map>
#include <unordered_set>
#include "client/UdpClient.h"
#include "client/client_map.h"

namespace client {

struct ArrayDiffResult {
  ClientMap added;
  ClientMap kept;
  ClientMap removed;
};

ArrayDiffResult ArrayDiff(ClientMap, ClientMap);

}  // namespace client

#endif  // FREEROAM_ARRAY_DIFF_H
