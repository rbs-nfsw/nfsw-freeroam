#include "stdafx.h"

#include <cstdlib>
#include <functional>
#include <iostream>
#include <system_error>
#include <unordered_map>

#include <gflags/gflags.h>

#include "backward.hpp"
#include "client/UdpClient.h"
#include "client/client_map.h"
#include "util/debug.h"
#include "util/endpoint.h"

#include <boost/asio/steady_timer.hpp>

DEFINE_int32(port, 9999, "Listen port");
DEFINE_string(replay, "", "Replay packet recording");

class Server {
 public:
  Server(boost::asio::io_service& io_service, short port,
         std::vector<std::vector<unsigned char>> replay)
      : socket_(io_service, boost::asio::ip::udp::endpoint(
                                boost::asio::ip::udp::v4(), port)),
        data_(1024),
        replay_(replay),
        cleanup_timer_(io_service, std::chrono::seconds(10)) {
    cleanup_timer_.async_wait(
        std::bind(&Server::CleanOldClients, this, std::placeholders::_1));
    do_receive();
  }

  void do_receive() {
    socket_.async_receive_from(
        boost::asio::buffer(data_), sender_endpoint_,
        [this](const boost::system::error_code ec, size_t bytes_recvd) {
          if (!ec && bytes_recvd > 0) {
            this->clients_mutex_.lock();
            auto data = std::vector<unsigned char>(data_.begin(),
                                                   data_.begin() + bytes_recvd);
            auto endpoint = util::endpoint_to_string(sender_endpoint_);
            auto client_iter = clients_.find(endpoint);
            if (client_iter == clients_.end()) {
              if (bytes_recvd == 58 && data[2] == 0x06) {
                spdlog::get("console")->info("New client from {}", endpoint);
                debugHex(data);
                uint16_t clitime1 = data[52];
                uint16_t clitime2 = data[53];
                uint16_t clitimem = clitime1 << 8 | clitime2;
                clients_[endpoint] = new client::UdpClient(
                    socket_, sender_endpoint_, &clients_, clitimem, replay_);
                clients_[endpoint]->SendHandshakeReply();
              }
            } else {
              handle((*client_iter).second, data);
            }
            this->clients_mutex_.unlock();
          }
          do_receive();
        });
  }

 private:
  boost::asio::ip::udp::socket socket_;
  boost::asio::ip::udp::endpoint sender_endpoint_;
  client::ClientMap clients_;
  std::mutex clients_mutex_;
  std::vector<unsigned char> data_;
  boost::asio::steady_timer cleanup_timer_;
  std::vector<std::vector<unsigned char>> replay_;

  void handle(client::UdpClient* cli, std::vector<unsigned char> in_data) {
    cli->process(in_data);
  }

  void CleanOldClients(const boost::system::error_code) {
    clients_mutex_.lock();
    auto iter = clients_.begin();
    while (iter != clients_.end()) {
      auto client = (*iter).second;
      if (!client->active()) {
        spdlog::get("console")->info("Removing old client {}",
                                     client->endpoint());
        delete client;
        iter = clients_.erase(iter);
      } else {
        ++iter;
      }
    }
    clients_mutex_.unlock();
    cleanup_timer_.expires_from_now(std::chrono::seconds(10));
    cleanup_timer_.async_wait(
        std::bind(&Server::CleanOldClients, this, std::placeholders::_1));
  }
};

int main(int argc, char* argv[]) {
  auto console = spdlog::stderr_color_st("console");
  try {
    gflags::ParseCommandLineFlags(&argc, &argv, true);

    boost::asio::io_service io_service;

    std::vector<std::vector<unsigned char>> replay;
    if (FLAGS_replay != "") {
      std::ifstream file(FLAGS_replay);
      std::string line;
      while (std::getline(file, line)) {
        if (line == "") continue;
        std::vector<unsigned char> replay_part;
        std::vector<std::string> hex_parts;
        boost::split(hex_parts, line, boost::is_any_of(": "));
        for (auto const& part : hex_parts) {
          if (part == "") continue;
          replay_part.push_back(strtol(part.c_str(), nullptr, 16));
        }
        std::vector<unsigned char>(replay_part.begin() + 16,
                                   replay_part.end() - 5)
            .swap(replay_part);
        replay.push_back(replay_part);
      }
      console->info("Will replay {} packets", replay.size());
    }

    console->info("Server started in port {}", FLAGS_port);
    Server s(io_service, FLAGS_port, replay);

    io_service.run();
  } catch (const std::exception& e) {
    console->critical("Exception has occurred:\n{}", e.what());
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
