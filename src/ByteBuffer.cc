#include "ByteBuffer.h"

#include <arpa/inet.h>

ByteBuffer::ByteBuffer() : array_() {}

void ByteBuffer::Write(std::vector<unsigned char> data) {
  for (unsigned char const& b : data) {
    WriteByte(b);
  }
}

void ByteBuffer::WriteByte(char b) { array_.push_back(b); }

void ByteBuffer::WriteUint16BE(uint16_t b) {
  WriteByte(b >> 8);
  WriteByte(b & 0xff);
}

std::vector<unsigned char> ByteBuffer::Bytes() { return array_; }