#ifndef FREEROAM_BYTEBUFFER_H_
#define FREEROAM_BYTEBUFFER_H_

#include "stdafx.h"

class ByteBuffer {
 public:
  ByteBuffer();
  void Write(std::vector<unsigned char>);
  void WriteByte(char);
  void WriteUint16BE(uint16_t);
  std::vector<unsigned char> Bytes();

 private:
  std::vector<unsigned char> array_;
};

#endif  // FREEROAM_BYTEBUFFER_H_