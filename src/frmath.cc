#include "stdafx.h"

//#include <cmath>
#include "frmath.h"

namespace math {

Vector2D Subtract(Vector2D a, Vector2D b) {
  return Vector2D{a.x - b.x, a.y - b.y};
}

Vector2D Absolute(Vector2D a) { return Vector2D{labs(a.x), labs(a.y)}; }

unsigned long Length(Vector2D a) { return sqrt(a.x * a.x + a.y * a.y); }

}  // namespace math