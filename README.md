# nfsw-freeroam

[Meson](http://mesonbuild.com/) is needed for build

## Dependencies
- spdlog `libspdlog-dev`
- asio `libasio-dev`
- boost `libboost-all-dev`